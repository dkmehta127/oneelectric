import {BrowserRouter,Routes,Route, HashRouter} from 'react-router-dom';
import React from "react";

import './App.css';
import Dashboard from './components/Dashboard';
import Ride from './components/Ride';

import { ToastContainer, toast, Slide } from "react-toastify";

function App() {
  return (
    <React.Fragment>
            <ToastContainer />

  <HashRouter>
  <Routes>
    <Route path='/' element={<Dashboard/>}/>
    <Route path='/ride' element={<Ride/>}/>
  </Routes>
  </HashRouter>
  </React.Fragment>
  
  );
}

export default App;
