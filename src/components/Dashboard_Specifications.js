import React from 'react';
import Shape1 from "../assets/images/Shape-1.png";
import Shape2 from "../assets/images/Shape-2.png";
import Shape3 from "../assets/images/Shape-3.png";

const Dashboard_Specifications = () => {
  return (
    <div>
      <div className="SpecificationsArea">
        <div className="SpecificationsLeftShape">
          <img src={Shape2} />
        </div>
        <div className="SpecificationsBox">
          <div className="row">
            <div className="col-lg-6 col-md-12">
              <div className="Specifications">
                <h4>Specifications</h4>
                <p>
                  <label>Top Speed</label>
                  <span>90 Kmph</span>
                </p>
                <p>
                  <label>Range / Charge</label>
                  <span>110+ Km Eco mode 80 Km Normal</span>
                </p>
                <p>
                  <label>Acceleration</label>
                  <span>0 to 60 Kmph : 7.5 Seconds</span>
                </p>
                <p>
                  <label>Torque Nm</label>
                  <span>200+</span>
                </p>
                <p>
                  <label>Motor</label>
                  <span>5.0 kW Peak Power</span>
                </p>
                <p>
                  <label>Lithium Battery</label>
                  <span>3.65 kWh</span>
                </p>
                <p>
                  <label>Seating Capacity</label>
                  <span>2</span>
                </p>
                <p>
                  <label>Combi Brakes System</label>
                  <span>Disc/Disc 240mm/220mm</span>
                </p>
                <p>
                  <label>Front Suspension</label>
                  <span>Telescopic Hydraulic</span>
                </p>
              </div>
            </div>
            <div className="col-lg-6 col-md-12">
              <div className="Specifications">
                <h4>Specifications</h4>
                <p>
                  <label>Front Suspension</label>
                  <span>Telescopic, Dia 30mm</span>
                </p>
                <p>
                  <label>Rear Suspension</label>
                  <span>Twin Shock Absorber</span>
                </p>
                <p>
                  <label>Front Wheel</label>
                  <span>80/100 17 inches Tubeless</span>
                </p>
                <p>
                  <label>Rear Wheel</label>
                  <span>120/80 17 inches Tubeless</span>
                </p>
                <p>
                  <label>Odometer </label>
                  <span>Digital</span>
                </p>
                <p>
                  <label>GPS/App Connect</label>
                  <span>Optional</span>
                </p>
                <p>
                  <label>Headlight</label>
                  <span>Halogen/LED (12V – 35W)</span>
                </p>
                <p>
                  <label>Brake/Taillight</label>
                  <span>12V – 5/21W (Multi Reflector)</span>
                </p>
                <p>
                  <label>DRL</label>
                  <span>Yes</span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="SpecificationsRightShape">
          <img src={Shape3} />
        </div>
      </div>
    </div>
  );
}

export default Dashboard_Specifications