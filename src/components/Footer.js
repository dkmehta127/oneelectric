import React from "react";
import Logo from "../assets/images/Logo.png";
import "../App.css";

const Footer = () => {
  return (
    <footer>
      <div className="Footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="Foots">
                <img src={Logo}/>
                <p>
                  With lots of unique blocks, you can easily build a page
                  without coding.
                </p>
              </div>
            </div>
            <div className="col-md-4 col-sm-6">
              <div className="Foots">
                <h4>Follow Us</h4>
                <ul>
                  <li>
                    <a href="javascript:void(0);">
                      <i className="fa fa-twitter" />
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <i className="fa fa-facebook" />
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <i className="fa fa-google" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-4 col-sm-6">
              <div className="Foots">
                <h4>Contact us</h4>
                <p>support@grayic.com</p>
                <p>+133-394-3439-1435</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="CopyRight">
        <p>
          <i className="fa fa-copyright" /> 2023 One Elctric, All Rights
          Reserved
        </p>
      </div>
    </footer>
  );
};

export default Footer;
