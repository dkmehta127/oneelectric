import { authConstant } from "../actionTypes";

const initialState = {
  adminData: {},
  loader: false,
  loginStatus: false,
  error: {},
};

export default function adminLoginReducer(
  state = initialState,
  { type, payload }
) {
  switch (type) {
    // LOGIN
    case authConstant.LOGIN_INITIATE:
      return { ...state, loader: true, loginStatus: false, adminData: {} };

    case authConstant.LOGIN_SUCCESS:
      return { ...state, loader: false, loginStatus: true, adminData: payload };

    case authConstant.LOGIN_FAILURE:
      return { ...state, loader: false, error: payload };

    // LOG OUT
    case authConstant.LOGOUT_INITIATE:
      return { ...state, loader: true };

    case authConstant.LOGOUT_SUCCESS:
      return { ...state, loader: false };

    case authConstant.LOGIN_FAILURE:
      return { ...state, loader: false, error: payload };

    //   SEND OTP

    case authConstant.LOGIN_OTP_INITIATE:
      return { ...state, loader: true };

    case authConstant.LOGIN_OTP_SUCCESS:
      return { ...state, loader: false };

    case authConstant.LOGIN_OTP_FAILURE:
      return { ...state, loader: false };

    //  VERIFY OTP

    case authConstant.VERIFY_OTP_INITIATE:
      return { ...state, loader: true };

    case authConstant.VERIFY_OTP_SUCCESS:
      return { ...state, loader: false };

    case authConstant.VERIFY_OTP_FAILURE:
      return { ...state, loader: false };

    // FORGET PASSWORD

    case authConstant.FORGET_PASSWORD_INITIATE:
      return { ...state, loader: true };

    case authConstant.FORGET_PASSWORD_SUCCESS:
      return { ...state, loader: false };

    case authConstant.FORGET_PASSWORD_FAILURE:
      return { ...state, loader: false };

    // UPDATE PASSWORD

    case authConstant.UPDATE_PASSWORD_INITIATE:
      return { ...state, loader: true };

    case authConstant.UPDATE_PASSWORD_SUCCESS:
      return { ...state, loader: false };

    case authConstant.UPDATE_PASSWORD_FAILURE:
      return { ...state, loader: false };

    // GET DETAILS

    case authConstant.ADMIN_DETAILS_INITIATE:
      return { ...state, loader: true, adminData: {} };

    case authConstant.ADMIN_DETAILS_SUCCESS:
      return { ...state, loader: false, adminData: payload };

    case authConstant.ADMIN_DETAILS_FAILURE:
      return { ...state, loader: false };

    // UPDATE DETAILS

    case authConstant.ADMIN_UPDATE_DETAILS_INITIATE:
      return { ...state, loader: true };

    case authConstant.ADMIN_UPDATE_DETAILS_SUCCESS:
      return { ...state, loader: false };

    case authConstant.ADMIN_UPDATE_DETAILS_FAILURE:
      return { ...state, loader: false };

    default:
      return state;
  }
}
