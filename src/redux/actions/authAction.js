
import { authConstant } from "../actionTypes";

import axios from "axios";
import { baseUrl } from "../../config/config";
import { isLoggedIn } from "../../utils";




//  LOGIN 

export const adminLoginInitiate=() =>({
    type:authConstant.LOGIN_INITIATE

})

export const adminLoginSuccess=(data) =>({
    type:authConstant.LOGIN_SUCCESS,
    payload:data

})

export const adminLoginFailure=(data) =>({
    type:authConstant.LOGIN_FAILURE,
    payload : data

})


export const  AdminLoginAction=(payload)=>{
    return dispatch => {
        dispatch(adminLoginInitiate())
        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/admin/login`, payload)
                .then(response => {
                    const data = response.data
                    if (data.code && data.status === 200) {
                        dispatch(adminLoginSuccess(data))

                    }
                    else {
                        dispatch(adminLoginFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(adminLoginFailure(err))
                    reject(err);
                })

        );

    }

}

//   LOG OUT


export const logoutUserInitiate=() =>({
    type: authConstant.LOGOUT_INITIATE

})

export const logoutUserSuccess=(data) =>({
    type:authConstant.LOGOUT_SUCCESS,
    payload:data

})

export const logoutUserFailure=(data) =>({
    type:authConstant.LOGOUT_FAILURE,
    payload : data

})



export const LogoutUserAction=(payload)=>{

    const token =isLoggedIn('adminData');
    console.log(token)
 
    
        
    return dispatch => {
        dispatch(logoutUserInitiate())

        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/admin/adminLogout`,payload,{
                headers: {authorization:`${token}`}})
                .then(response => {
                    const data = response.data
                    if (data && data.status === 200) {
                        dispatch(logoutUserSuccess(data))

                    }
                    else {
                        dispatch(logoutUserFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(logoutUserFailure(err))
                    reject(err);
                })

        );
    }

}




// SEND OTP




export const loginOtpInitiate=() =>({
    type:authConstant.LOGIN_OTP_INITIATE

})

export const loginOtpSuccess=(data) =>({
    type:authConstant.LOGIN_OTP_SUCCESS,
    payload:data

})

export const loginOtpFailure=(data) =>({
    type:authConstant.LOGIN_OTP_FAILURE,
    payload : data

})



export const  LoginOtpAction=(payload)=>{
    
        
    return dispatch => {
        dispatch(loginOtpInitiate())
        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/admin/sendOtpOnEmail`, payload)
                .then(response => {
                    const data = response.data
                    if (data.code && data.status === 200) {
                        dispatch(loginOtpSuccess(data))

                    }
                    else {
                        dispatch(loginOtpFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(loginOtpFailure(err))
                    reject(err);
                })

        );
    }

}




// UPDATE PASSWORD




export const updatePasswordInitiate=() =>({
    type: authConstant.UPDATE_PASSWORD_INITIATE

})

export const updatePasswordSuccess=(data) =>({
    type:authConstant.UPDATE_PASSWORD_SUCCESS,
    payload:data

})

export const updatePasswordFailure=(data) =>({
    type:authConstant.UPDATE_PASSWORD_FAILURE,
    payload : data

})




export const UpdatePasswordAction=(payload)=>{

    const token =isLoggedIn('adminData');

    
        
    return dispatch => {
        dispatch(updatePasswordInitiate())

        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/admin/changePassword`,payload,{
                headers: { Authorization: `${token}`},})
                .then(response => {
                    const data = response.data
                    if (data && data.status === 200) {
                        dispatch(updatePasswordSuccess(data))

                    }
                    else {
                        dispatch(updatePasswordFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(updatePasswordFailure(err))
                    reject(err);
                })

        );
    }

}




//  FORGET PASSWORD



export const forgetPasswordInitiate=() =>({
    type: authConstant.FORGET_PASSWORD_INITIATE

})

export const forgetPasswordSuccess=(data) =>({
    type:authConstant.FORGET_PASSWORD_SUCCESS,
    payload:data

})

export const forgetPasswordFailure=(data) =>({
    type:authConstant.FORGET_PASSWORD_FAILURE,
    payload : data

})




export const  ForgetPasswordAction=(payload)=>{
    
        
    return dispatch => {
        dispatch(forgetPasswordInitiate())
        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/admin/forgetPassword`, payload)
                .then(response => {
                    const data = response.data
                    if (data.code && data.status === 200) {
                        dispatch(forgetPasswordInitiate(data))

                    }
                    else {
                        dispatch(forgetPasswordFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(forgetPasswordFailure(err))
                    reject(err);
                })

        );
    }

}



// GET DETAILS

export const adminDetailsInitiate=() =>({
    type: authConstant.ADMIN_DETAILS_INITIATE

})

export const adminDetailstSuccess=(data) =>({
    type:authConstant.ADMIN_DETAILS_SUCCESS,
    payload:data

})

export const adminDetailstFailure=(data) =>({
    type: authConstant.ADMIN_DETAILS_FAILURE,
    payload : data

})




export const  AdminDetailsAction=(payload)=>{
    const token =isLoggedIn('adminData');

    
        
    return dispatch => {
        dispatch(adminDetailsInitiate())
        return new Promise((resolve, reject) =>
        axios.post(baseUrl+'/admin/adminDetails', payload,{
            headers: {authorization:`${token}`}})
                .then(response => {
                    const data = response.data
                    if (data && data.status === 200) {
                        dispatch(adminDetailstSuccess(data))

                    }
                    else {
                        dispatch(adminDetailstFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(adminDetailstFailure(err))
                    reject(err);
                })

        );
    }

}



export const adminUpdateDetailsInitiate=() =>({
    type: authConstant.ADMIN_UPDATE_DETAILS_INITIATE

})

export const adminUpdateDetailstSuccess=(data) =>({
    type:authConstant.ADMIN_UPDATE_DETAILS_SUCCESS,
    payload:data

})

export const adminUpdateDetailstFailure=(data) =>({
    type: authConstant.ADMIN_UPDATE_DETAILS_FAILURE,
    payload : data

})



// UPDATE DETAILS


export const  AdminUpdateDetailsAction=(payload)=>{
    const token =isLoggedIn('artisanData');

    
        
    return dispatch => {
        dispatch(adminUpdateDetailsInitiate())
        return new Promise((resolve, reject) =>
            axios.post(`${baseUrl}/artisian/updateArtisan`, payload,{
                headers: {authorization:`${token}`}})
                .then(response => {
                    const data = response.data
                    if (data && data.status === 200) {
                        dispatch(adminUpdateDetailstSuccess(data))

                    }
                    else {
                        dispatch(adminUpdateDetailstFailure(data))
                    }
                    resolve(data)
                }).catch(err => {
                    dispatch(adminUpdateDetailstFailure(err))
                    reject(err);
                })

        );
    }

}

