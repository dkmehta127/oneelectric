export function isLoggedIn(adminType) {
    let session = getObject(adminType) || {};
    session = Object.keys(session).length && JSON.parse(session);
    let accessToken = session && session && session['jwtToken'] || "";
    return accessToken;
}

export function getObject(key) {
  //  console.log('keye', key,window.localStorage.getItem(key))
    if (window && window.localStorage) {
      return window.localStorage.getItem(key);
    }
  }

  export function multiPartData(data){
    let multiPart = new FormData();
    for (let key in data){
      multiPart.append(key,data[key])
    }
  
    return multiPart
  }